window.addEventListener('DOMContentLoaded', () => {
	const keys = document.getElementsByClassName('keys')[0];
	const ravno = document.getElementsByClassName('orange')[0];
	const text = document.querySelector('.display > input');
	let rezult = '';
	let remember = '';
	let calc = {
		op1: '',
		op2: '',
		op3: ''
	};

	function calculate (calc) {
		let a = '';
		let b = '';
		if (calc.op1 == ''){
			a = 0;
		} else {
			a = parseFloat(calc.op1);
		}
		if (calc.op3 == ''){
			b = parseFloat(calc.op1);
		} else {
			b = parseFloat(calc.op3);
		}

		switch (calc.op2) {
			case '+':
				return a + b;
			case '-':
				return a - b;
			case '*':
				return a * b;
			case '/':
				return a / b;
		}
	}

	keys.addEventListener('click', function (e) {
		if (e.target.value) {
			if (e.target.value.search(/[0-9.]/) != -1 && calc.op2 == '') {
				if (rezult != ''){
					rezult = '';
				}
				calc.op1 += e.target.value;
				text.value = calc.op1;
			}
			if (e.target.value.search(/[0-9.]/) != -1 && calc.op2 != '') {
				calc.op3 += e.target.value;
				text.value = calc.op3;
				ravno.disabled = false;
			}
			if (e.target.value.search(/[-+*/]/) != -1 &&
				e.target.value.search(/m\+/) == -1 &&
				e.target.value.search(/m\-/) == -1) {
				calc.op2 = e.target.value;
				calc.op3 = '';
			}
			if (e.target.value.search('=') != -1) {
				if (rezult != '' && calc.op1 == '') {
					calc.op1 = rezult;
				}
				rezult = calculate(calc);
				text.value = rezult;
				calc.op1 = '';
				calc.op2 = '';
				ravno.disabled = true;
			}
			if (e.target.value.search('C') != -1) {
				rezult = '';
				text.value = '';
				calc.op1 = '';
				calc.op2 = '';
				calc.op3 = '';
				ravno.disabled = true;
			}
			if (e.target.value.search(/m\+/) != -1) {
				remember = text.value;
			}
			if (e.target.value.search(/m\-/) != -1) {
				remember = '';
			}
			if (e.target.value.search(/\mrc/) != -1) {
				if (calc.op2 == ''){
					calc.op1 = remember;
				} else {
					calc.op3 = remember;
					ravno.disabled = false;
				}
				text.value = remember;
			}
			console.log(calc);
		}		
	});
})
